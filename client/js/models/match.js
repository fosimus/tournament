'use strict';

import commonApi from '../api/index.js';

class Match {
    constructor(tournamentId, roundId, match) {
        this.tournamentId = tournamentId;
        this.roundId = roundId;
        this.matchId = match.match;
        this.teamIds = match.teamIds;
        this.score = null;
        this.winnerTeam = null;
    }

    get winnerTeamForMatch() {
        return this.winnerTeam;
    }

    renderMatch(action) {
        window.dispatchEvent(
            new CustomEvent(action)
        );
    }

    async getDataForEachTeam(teamsIds) {
        let teamsData = [];

        await Promise.all(
            teamsIds.map(async teamId => {
                const { score, name } = await commonApi.getTeam(this.tournamentId, teamId);

                teamsData.push({ teamId, score, name });
            })
        );

        return teamsData;
    }

    async getWinnerForMatch() {
        // requestsManager helps to avoid performance issue in browsers by splitting requests
        const teamsData = await commonApi.requestsManager({
            requests: this.teamIds,
            callback: this.getDataForEachTeam.bind(this)
        });

        // get winner score from server according to the teamsData
        const { score } = await commonApi.getWinnerScore(this.tournamentId, teamsData, this.score);
        // filter teams by score from server
        const winners = teamsData.filter(team => team.score === score);
        // there is a chance that several teams can have the same win-score
        // according to the README.md ¯\_(ツ)_/¯ team with the lowest ID is the winner
        if (winners.length > 1) {
            winners.sort((a, b) => a.teamId - b.teamId);
        }

        return winners[0];
    }

    async play() {
        const { score } = await commonApi.playMatch(this.tournamentId, this.roundId, this.matchId);
        // mark match as played but the winner is not found yet
        this.renderMatch('paint_played_match');
        this.score = score;
        // find winner for current match
        this.winnerTeam = await this.getWinnerForMatch();
        this.renderMatch('paint_finished_match');
    }
};

export default Match;
