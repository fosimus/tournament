'use strict';

import commonApi from '../api/index.js';
import RoundManager from '../managers/roundManager.js';
import Round from './round.js';
import Match from './match.js';

class Tournament {
    constructor(tournamentId, matchUps, teamsPerMatch, numberOfTeams) {
        this.tournamentId = tournamentId;
        this.teamsPerMatch = teamsPerMatch;
        this.numberOfTeams = numberOfTeams;
        this.firstMatches = matchUps;
        this.rounds = [];
        this.numberOfRounds = Tournament.getNumberOfRounds(this.teamsPerMatch, this.numberOfTeams);
        this.numberOfMatches = Tournament.getNumberOfMatches(this.teamsPerMatch, this.numberOfTeams, this.numberOfRounds);
        this.winnerTeam = null;
    }

    // same function on server side to get number of rounds
    static getNumberOfRounds(teamsPerMatch, numberOfTeams) {
        let rounds = 1;
        let teamCount;

        for(teamCount = teamsPerMatch; teamCount < numberOfTeams; teamCount *= teamsPerMatch) {
            rounds++;
        }

        return teamCount === numberOfTeams ? rounds : null;
    }

    static getNumberOfMatches(teamsPerMatch, numberOfTeams, numberOfRounds) {
        let numberOfMatches = 0;
        let numberOfMatchesThisRound = numberOfTeams;

        for (let round = 0; round < numberOfRounds; round++) {
            numberOfMatches += numberOfMatchesThisRound /= teamsPerMatch;
        }

        return numberOfMatches;
    }

    renderMatches() {
        window.dispatchEvent(
            new CustomEvent("show_loader")
        );
        window.dispatchEvent(
            new CustomEvent("show_matches", { detail: this.numberOfMatches })
        );
    }

    renderWinner(name) {
        window.dispatchEvent(
            new CustomEvent("show_winner", { detail:  name })
        );
    }

    resetForm() {
        window.dispatchEvent(
            new CustomEvent("reset_form")
        );
    }

    async start() {
        try {
            const initialMatches = this.firstMatches;
            this.renderMatches();

            const roundManager = new RoundManager(this.tournamentId, this.numberOfRounds, this.teamsPerMatch, initialMatches, Round, Match);

            await roundManager.start();

            this.renderWinner(roundManager.winnerTeam.name);
            this.resetForm();

        } catch(error){
            throw new Error(`Tournament error. ${error.message}`);
        }
    }
}

export default Tournament;
