'use strict';

import TournamentFactory from './factories/tournament.js';
import RenderEvents from './render/index.js';

window.onload = () => {
    const teamsPerMatchElement = document.querySelector('#teamsPerMatch');
    const numberOfTeamsElement = document.querySelector('#numberOfTeams');
    const matchesElement = document.querySelector('#matches');
    const winnerElement = document.querySelector('#winner');
    // create events for rendering tournament data: winner, matches, loader
    RenderEvents.init({
        matches: matchesElement,
        winner: winnerElement,
        teamsPerMatch: teamsPerMatchElement,
        numberOfTeams: numberOfTeamsElement
    });

    document.querySelector('#start').onclick = async () => {
        const teamsPerMatch = parseInt(teamsPerMatchElement.value);
        const numberOfTeams = parseInt(numberOfTeamsElement.value);
        // remove previous winner and matches | render event
        window.dispatchEvent(
            new CustomEvent("reset_view")
        );

        const tournamentFactory = new TournamentFactory();
        const tournament = await tournamentFactory.create(teamsPerMatch, numberOfTeams);
        tournament.start();
    };
};
