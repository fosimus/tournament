'use strict';

import commonApi from '../api/index.js';

class Round {
    constructor(tournamentId, roundId, matches, teamsPerMatch, matchClass) {
        this.tournamentId = tournamentId;
        this.roundId = roundId;
        this.matches = matches;
        this.teamsPerMatch = teamsPerMatch;
        this.winners = [];
        this.matchClass = matchClass;
    }

    get winnerTeams(){
        return this.winners;
    }

    async playMatches(matches) {
        let winners = [];

        await Promise.all(
            // play one match
            matches.map(async matchItem => {
                const match = new this.matchClass(this.tournamentId, this.roundId, matchItem);

                await match.play();
                // get winner for current match
                winners.push(match.winnerTeamForMatch);
            })
        );

        return winners;
    }

    async play() {
        // requestsManager helps to avoid performance issue in browsers by splitting requests
        this.winners = await commonApi.requestsManager({
            requests: this.matches, // array for requests
            callback: this.playMatches.bind(this) // fn which will be called for each bunch of requests
        });
    }
};

export default Round;
