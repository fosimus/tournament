'use strict';

import commonApi from '../api/index.js';
import Tournament from '../models/tournament.js';

class TournamentFactory {
    async create(teamsPerMatch, numberOfTeams) {
        // validate inputs
        if(!teamsPerMatch || !numberOfTeams) {
            throw new Error('Validation is failed. Please, put correct data');
        }
        try {
            // get tournament data and matches for first round
            const { tournamentId, matchUps } = await commonApi.getTournamentData(teamsPerMatch, numberOfTeams);

            return new Tournament(tournamentId, matchUps, teamsPerMatch, numberOfTeams);

        } catch(error) {
            throw new Error(`Cannot start tournament. ${error.message}`);
        }
    }
};

export default TournamentFactory;
