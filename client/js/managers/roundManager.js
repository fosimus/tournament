'use strict';

import commonApi from '../api/index.js';

class RoundManager {
    constructor(tournamentId, numberOfRounds, teamsPerMatch, initialMatches, Round, Match) {
        this.tournamentId = tournamentId;
        this.numberOfRounds = numberOfRounds;
        this.teamsPerMatch = teamsPerMatch;
        this.initialMatches = initialMatches;
        this.roundClass = Round;
        this.matchClass = Match;
        this.winner = null;
    }

    get winnerTeam(){
        return this.winner;
    }

    static createMatchesFromWinners(winners, teamsPerMatch) {
        let teamsInMatch = [];
        let roundMatches = [];
        // create special structure for API
        for (let i = 0; i < winners.length; i++) {
            teamsInMatch.push(winners[i].teamId);
            if (teamsInMatch.length === teamsPerMatch) {
                roundMatches.push({
                    match: roundMatches.length,
                    teamIds: teamsInMatch.splice(0)
                });
            }
        }

        return roundMatches;
    }

    async start() {
        let roundMatches = this.initialMatches;
        // play all rounds
        for (let roundId = 0; roundId < this.numberOfRounds; roundId++) {
            const isFinalRound = (roundId === this.numberOfRounds - 1);
            const round = new this.roundClass(this.tournamentId, roundId, roundMatches, this.teamsPerMatch, this.matchClass);

            await round.play();

            if (isFinalRound) {
                // get winner from last round and last round must contain just one winner team
                this.winner = round.winnerTeams[0];
                roundMatches = null;
            } else {
                // prepare some data for the next round (special structure for API)
                roundMatches = RoundManager.createMatchesFromWinners(round.winners, this.teamsPerMatch);
            }
        }
    }
};

export default RoundManager;
