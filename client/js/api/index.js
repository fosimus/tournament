'use strict';

const commonApi = {
    async getTournamentData(teamsPerMatch, numberOfTeams) {
        return await this._makeRequest({
            method: 'POST',
            url: '/tournament',
            body: `numberOfTeams=${numberOfTeams}&teamsPerMatch=${teamsPerMatch}`
        });
    },

    async playMatch(tournamentId, roundId, matchId) {
        return await this._makeRequest({
            method: 'GET',
            url: `/match?tournamentId=${tournamentId}&round=${roundId}&match=${matchId}`
        });
    },

    async getTeam(tournamentId, teamId) {
        return await this._makeRequest({
            method: 'GET',
            url: `/team?tournamentId=${tournamentId}&teamId=${teamId}`
        });
    },

    async getWinnerScore(tournamentId, teamsData, matchScore) {
        const scoresUrl = teamsData.map(team => `&teamScores=${team.score}`).join('');

        return await this._makeRequest({
            method: 'GET',
            url: `/winner?tournamentId=${tournamentId}${scoresUrl}&matchScore=${matchScore}`
        });
    },
    // split requests (performance issue)
    async requestsManager({ requests, callback }) {
        let result = [];
        const MAX_REQUESTS = 1000;
        const steps = Math.ceil(requests.length / MAX_REQUESTS);
        const allRequests = [...requests];

        for(let i = 1; i <= steps; i++) {
            const bunchOfRequests = allRequests.splice(0, MAX_REQUESTS);

            result = result.concat(
                await callback(bunchOfRequests)
            );
        };

        return result;
    },

    async _makeRequest({ method, url, body }) {
        let options = {};
        if(method === 'POST') {
            options = {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                body
            }
        }
        const request = await fetch(url, options);
        const json = await request.json();

        if(json && json.error) {
            return Promise.reject(json);
        }
        return Promise.resolve(json);
    }
};

export default commonApi;
