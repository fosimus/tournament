'use strict';

// describe all events for rendering

const RenderEvents = {
    init(elements) {
        const { matches, winner, teamsPerMatch, numberOfTeams } = elements;

        window.addEventListener("reset_view", function(e) {
            winner.innerHTML = '';
            matches.innerHTML = '';
        });

        window.addEventListener("reset_form", function(e) {
            teamsPerMatch.value = '';
            numberOfTeams.value = '';
        });

        window.addEventListener("show_winner", function(e) {
            winner.innerHTML = e.detail;
        });

        window.addEventListener("show_loader", function(e) {
            winner.innerHTML = '<div class="loader"></div>';
        });

        window.addEventListener("show_matches", function(e) {
            const numberOfMatches = e.detail;
            let html = '';

            for(let i = 1; i <= numberOfMatches; i++) {
                html += '<div class="matches__match not-played"></div>';
            };
            matches.innerHTML = html;
        });

        window.addEventListener("paint_played_match", function(e) {
            const match = matches.querySelector('.matches__match.not-played');

            if (match) {
                match.classList.remove('not-played');
                match.classList.add('played');
            }
        });

        window.addEventListener("paint_finished_match", function(e) {
            const match = matches.querySelector('.matches__match.played');

            if (match) {
                match.classList.remove('played');
                match.classList.add('finished');
            }
        });
    }
};

export default RenderEvents;
